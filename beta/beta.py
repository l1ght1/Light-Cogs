from discord.ui import Button, ButtonStyle, View
from redbot.core import commands

class Beta(commands.Cog):
    """My custom cog"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def buttons(self, ctx):
      button = Button(label='DXVK', style=discord.ButtonStyle.url, url='https://github.com/lutris/docs/blob/master/HowToDXVK.md')
      view = View()
      view.add_item(button)
      await ctx.send('Button!', view=view)

def setup(bot):
    bot.add_cog(Beta(bot))
