import discord

from redbot.core import commands

class ExtendedAdmin(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        
    @commands.command()
    @commands.has_permissions(administrator=True)
    async def say(self, ctx, *, message):
        """Says what you want it to say, what did you think?"""
        if not message:
            await ctx.send("Please specify a message to send.")
            return
        await ctx.message.delete()
        await ctx.send(message)

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def staffannounce(self, ctx, *, message):
        """Make an announcement for staff!"""
        if not message:
            await ctx.send("Please specify the announcement message.")
            return
        await ctx.message.delete()
        embed=discord.Embed(title="Attention, Staff!", description=message, color=0xe74c3c)
        await ctx.send(embed=embed)
    
    @commands.command()
    @commands.has_permissions(administrator=True)
    async def embedannounce(self, ctx, *, message):
        """Make an announcement!"""
        if not message:
            await ctx.send("Please specify the announcement message.")
            return
        await ctx.message.delete()
        embed=discord.Embed(title="Announcement!", description=message, color=0xe74c3c)
        await ctx.send(embed=embed)
     
    @commands.command()
    @commands.has_permissions(administrator=True)
    async def embed(self, ctx, *, message):
        """Send an embed!"""
        if not message:
            await ctx.send("Please specify your message.")
            return
        await ctx.message.delete()
        embed=discord.Embed(description=message, color=0xe74c3c)
        await ctx.send(embed=embed)
    
    @commands.command(aliases=['cl'])
    @commands.has_permissions(administrator=True)
    async def changelog(self, ctx, *, message):
        """Make a change and log it with this command!"""
        if not message:
            await ctx.send("Please enter a change.")
            return
        await ctx.message.delete()
        embed=discord.Embed(title="Changes Made:", description=message, color=0xe74c3c)
        embed.set_footer(text='Check time posted for change time.')
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(ExtendedAdmin(bot))
